package com.basicprograms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddList {

	public static void main(String args[]) {
	
	 List<Integer> list1 = new ArrayList<>();
	 List<Integer> list2 = new ArrayList<>();
	 List<Integer> newList = new ArrayList<>();
	 
	 list1.add(1);
	 list1.add(2);
	 list1.add(4);
	 
	 list2.add(1);
	 list2.add(3);
	 list2.add(4);
	 
	 for(int i=0; i<list1.size(); i++) {
	 newList.add(list1.get(i));
	 }
	 
	 for(int j=0; j<list2.size();j++) {
		 newList.add(list2.get(j));	 
	 }
	
	 Collections.sort(newList);
	 System.out.println(newList);
}
}