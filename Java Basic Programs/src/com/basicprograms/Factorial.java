package com.basicprograms;

public class Factorial {

	public static int Factorial(int num) {
		
		if (num >= 1) {
			return num * Factorial(num - 1);}
			else 
				return 1;
	}
	
	public static void main(String args[]) {
		int fact= Factorial(5);
		System.out.println(fact);
		
	}
}
