package com.basicprograms;

public class FibonacciRec {

	static int a=0, b=1, c;

	public static void recursion(int num) {
		
		if(num>=1) {
			c=a+b;
			System.out.print(" " +c);
			a=b;
			b=c;
			recursion(num-1);
			
		}
	}
	
	public static void main(String args[]) {
		System.out.print(a+ " " +b);
		recursion(10);
	
		
	}
}
