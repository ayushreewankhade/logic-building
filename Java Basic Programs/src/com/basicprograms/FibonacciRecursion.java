package com.basicprograms;

public class FibonacciRecursion {
	
	 static int n1=0, n2=1, n3;
	 
public void Fibonacci(int a) {
	
	 	  if(a>=1) {
		 n3=n1+n2;
		 System.out.print(" "+n3);
		 n1=n2;
		 n2=n3;
		 Fibonacci(a-1);
	 }}

public static void main(String args[]) {
	System.out.print(n1+" "+n2);
	FibonacciRecursion obj = new FibonacciRecursion();
	 obj.Fibonacci(10);
}}
