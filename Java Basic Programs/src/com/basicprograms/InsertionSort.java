package com.basicprograms;

public class InsertionSort {

	void sort(int arr[]) {
		
		for(int i=1; i<arr.length;i++) {
			int current =arr[i];
			int j=i-1;
			/* Move elements of arr[0..i-1], that are greater than key, 
			 * to one position ahead of their current position */
			while(j>=0 && current<arr[j]) {
				arr[j+1]=arr[j];
				j--;
			}
			arr[j+1]=current;	
		}
	}	
	static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
 
        System.out.println();
    }
    public static void main(String args[])
    {
        int arr[] = { 7,8,1,2,3 };
        InsertionSort ob = new InsertionSort();
        ob.sort(arr);
        printArray(arr);
    }
}
