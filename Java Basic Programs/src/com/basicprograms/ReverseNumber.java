package com.basicprograms;

import java.util.Scanner;

public class ReverseNumber {
	public static void main(String args[]) {
		Scanner sc= new Scanner(System.in);
		int reverse=0;
	for(int number=sc.nextInt()  ;number != 0; number=number/10)   
	{  
	int remainder = number % 10;  
	reverse = reverse * 10 + remainder;  
	}  
	System.out.println("The reverse of the given number is: " + reverse);  
	}  
}
