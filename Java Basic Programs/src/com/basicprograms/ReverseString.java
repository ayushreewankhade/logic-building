package com.basicprograms;

public class ReverseString {
public static void main(String args[]) {
String str = "radar", reverseStr = "";
    
    int strLength = str.length();

    for (int i = (strLength - 1); i >=0; --i) {
      reverseStr = reverseStr + str.charAt(i);
    }
    System.out.println("reverse of string is = " +reverseStr);
    if (str.equals(reverseStr)) {
      System.out.println(str + " is a Palindrome String.");
    }
    else {
      System.out.println(str + " is not a Palindrome String.");
    }
  }}