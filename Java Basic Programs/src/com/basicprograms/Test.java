package com.basicprograms;

//even on even index and odd on odd index

public class Test {
	
	public static void main(String args[]) {
		int esum=0;
		int osum=0;
		int arr[]= {1,3,5,6,20,22,0,11,26};
		for(int i=0;i<arr.length;i++ ) {
			if(i%2==0 && arr[i]%2==0) {
				esum+=arr[i];
				
				System.out.println("number " +arr[i] +" and index " +i +" is even");	
			}
			
			if(i%2!=0 && arr[i]%2!=0) {
				osum+=arr[i];
				
				System.out.println("number " +arr[i] +" and index " +i +" is odd");
			}
			
		}System.out.println("sum of even numbers at even index is " +esum);
		System.out.println("sum of odd numbers at odd index is " +osum);
	}

}
