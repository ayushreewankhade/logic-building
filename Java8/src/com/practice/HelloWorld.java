package com.practice;

public class HelloWorld {
    public static void main(String[] args) {
       String str= "Hello world";
       int countOfUpperCase=0;
       int countOfLowerCase=0;
       for(int i=0;i<str.length(); i++){
           if(Character.isUpperCase(str.charAt(i))){
               countOfUpperCase++;
           }
           else if(Character.isLowerCase(str.charAt(i))){
               countOfLowerCase++;
           }
       }
       System.out.println("count of upper case= " +countOfUpperCase + " Count of lower case= " +countOfLowerCase);
    }
}