package com.practice;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamAPI {

	public static void main(String args[]) {
		List<Integer> list = Arrays.asList(4,1,1,2,2,5,3,7,8,9,10);
		List<String> list6 = Arrays.asList("roshan", "", "hey", "balaji", "");


		List<Integer> list1= list.stream().sorted(Comparator.reverseOrder()).toList();
		List<Integer> list2= list.stream().sorted().toList();
		List<Integer> list3= list.stream().distinct().toList();
		List<Integer> list4= list.stream().filter(n -> n% 2==0).toList();
		List<Integer> list5=list.stream().collect(Collectors.groupingBy(n -> n, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey).toList();
		int sum = list.stream().reduce((a,b)-> a+b).get(); 
		List<String> list7= list6.stream().filter(string -> !string.isEmpty()).toList();	
		
		System.out.println(list1);
		System.out.println(list2);
		System.out.println(list3);
		System.out.println(list4);
		System.out.println(list5);
		System.out.println(sum);
		System.out.println(list7);
	}
}
