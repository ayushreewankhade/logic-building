package com.package1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnagramArray {

	public static void main(String args[]) {
		String[] arr={"eat","tea","tan","ate","nat","bat"};
		
		Map<String , List<String>> map=new HashMap<>();
		
				for(int i=0; i<arr.length; i++) {
					char[] ch= arr[i].toCharArray();
					Arrays.sort(ch);
				    String str=new String(ch);
				    
				    if(map.containsKey(str)) {
				    	map.get(str).add(arr[i]);
				    }else {
						List<String> list = new ArrayList<String>();

				    	list.add(arr[i]);
				    	map.put(str, list);
				    }
			
		}
				System.out.println(map.values());
	}
}
