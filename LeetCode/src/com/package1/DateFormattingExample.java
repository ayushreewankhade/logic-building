package com.package1;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class DateFormattingExample {
    public static void main(String[] args) {
        String dateString = "21/01/23";
        
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd/MM/yy");
        
        LocalDate date = LocalDate.parse(dateString, inputFormatter);
        
        String formattedDate = date.getDayOfMonth() + getDayOfMonthSuffix(date.getDayOfMonth())
                + " " + date.getMonth().getDisplayName(TextStyle.FULL, Locale.US)
                + " " + date.getYear();
        
        System.out.println(formattedDate);
    }
    
    // Helper method to get the appropriate suffix for the day of the month
    private static String getDayOfMonthSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }
}
