package com.package1;
/* WAP to find number whose index is equal to the sum of elements to its left and right*/
public class Demo {
//	public static void main(String[] args) {
//
//		int[] arr = { 3, 4, 2, 5, 1, 7, 1 };
//		int indexofNum;
//		int leftElement;
//		int rightElement;
//		int sum;
//		for (int i = 0; i < arr.length; i++) {
//			indexofNum = i;
//			leftElement = arr[i-1];
//			rightElement = arr[i+1];
//			sum= leftElement + rightElement ;
//
//			if (sum == indexofNum) {
//				System.out.println(arr[i]);
//			}
//		}
//
//	}

	public static void main(String[] args) {

		int[] arr = { 3, 4, 2, 5, 1, 7, 1 };

		for (int i = 0; i < arr.length; i++) {
			int indexofNum = i;
			int leftElement = (i > 0) ? arr[i - 1] : -1; // Use -1 to indicate an invalid index
			int rightElement = (i < arr.length - 1) ? arr[i + 1] : -1; // Use -1 to indicate an invalid index
			int sum = leftElement + rightElement;

			if (sum == indexofNum) {
				System.out.println(arr[i]);
			}
		}
	}

}
