package com.package1;

import java.util.HashMap;
import java.util.Map;

import java.util.HashMap;
import java.util.Map;

public class Frequency {

    public static void main(String[] args) {
        String str = "engineer";
        Map<Character, Integer> map = new HashMap<>();
        
        for(int i= 0; i<str.length();i++) {
        	if(map.containsKey(str.charAt(i))) {
        		map.put(str.charAt(i), map.get(str.charAt(i)) +1);
        	}
        	else {
        		map.put(str.charAt(i), 1);
        	}
        }
      map.forEach((a,b) -> System.out.println(a +" occurs "+ b +" times"));

	    }
}


