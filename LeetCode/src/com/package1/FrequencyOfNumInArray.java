package com.package1;

import java.util.HashMap;
import java.util.Map;

public class FrequencyOfNumInArray {

	public static void main(String args[]) {
		
		int[] arr= {1,6,1,4,1,2,1,2,3,4,5,3,4,5};
		Map<Integer, Integer> map= new HashMap<Integer, Integer>();
		for(int i=0; i<arr.length; i++) {
			if(map.containsKey(arr[i])) {
			map.put(arr[i], map.get(arr[i])+1);
			}else {
			map.put(arr[i], 1);
			}
		}
		map.forEach((a,b) -> System.out.println(a +" = "+b));

	}
}

