package com.package1;

public class FrequencyOfUpperLowerCase {

	public static void main(String args[]) {
		String str = "Hello World" ;
		int countOfUpperCase=0;
		int countOfLowerCase=0;
		for (int i= 0; i<str.trim().length();i++) {
			if(Character.isUpperCase(str.charAt(i))) {
				countOfUpperCase++;
			}
			else if(Character.isLowerCase(str.charAt(i))) {
				countOfLowerCase++;
			}
		}
		System.out.println(countOfUpperCase);
		System.out.println(countOfLowerCase);
	}
}
