package com.package1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveDuplicates {

	public static void main(String args[]) {
		int[] arr= {1,1,2,3,4,5,6,3,5};
		List<Integer> list= new ArrayList<>();
		List<Integer> list1= new ArrayList<>();
		List<Integer> list2= new ArrayList<>();

		for(int i=0; i<arr.length; i++) {
			list.add(arr[i]);
		}
		
		for(int j=0; j<list.size();j++) {
			if(!list1.contains(list.get(j))) {
				list1.add(list.get(j));
			}
			else {
				list2.add(list.get(j));
			}
		}
		System.out.println("unique elements: " +list1);
		System.out.println("duplicate elements: " +list2);

	}
}
