package com.package1;

public class ReverseNumber {

	public static void main(String args[]) {
		int num=12345;
		int revNum=0;
		
		while(num!=0) {
			int remainder = num % 10;
			revNum = revNum * 10 + remainder;
			num = num /10;
		}
		System.out.println(revNum);
	}
}
