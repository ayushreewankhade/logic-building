package com.package1;

import java.util.HashMap;
import java.util.Map;

public class RomanInteger {

	public static void main(String[] args) {
		
		String str= "DCCXCIX";
		int result =0;
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		map.put('I', 1);
		map.put('V', 5);
		map.put('X',10);
		map.put('L', 50);
		map.put('C', 100);
		map.put('D', 500);
		map.put('M',1000);
		
		for(int i=0; i<str.length(); i++) {
			if(i<str.length()-1 && map.get(str.charAt(i)) < map.get(str.charAt(i + 1))) {
				result -= map.get(str.charAt(i));
			}
			else
			result += map.get(str.charAt(i));
		
		}
		System.out.println(result);
		
	}
	
}
