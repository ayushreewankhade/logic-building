package com.package1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StringStartsWithH {
	
    public static void main(String args[]) {
	
	List<String> students = new ArrayList<>();
	students.add("Ashish");
	students.add("balaji"); 
	students.add("Shailesh");
	students.add("shamali");
	students.add("shashi"); 
	students.add("Arun"); 
	students.add("roshan"); 
	students.add("sanket");
	
	List<String> list= students.stream().filter(s-> s.toLowerCase().startsWith("s")).collect(Collectors.toList());
	System.out.println(list);
}}
