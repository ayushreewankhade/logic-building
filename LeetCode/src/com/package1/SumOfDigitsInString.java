package com.package1;

public class SumOfDigitsInString {

public static void main(String args[]) {
		
		String str = "111";
		int sum=0;
		for(int i=0;i<str.length();i++) {
			char ch = str.charAt(i);
			int j= Character.getNumericValue(ch);
			sum +=j;
		}
		System.out.println(sum);
	}
}
