package com.package1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SumOfSquareOfPrimeNum {

	    public static void main(String[] args) {
	        int n = 4; // Change this value to the desired number of primes
	        int count = 0;
	        long sum = 0;
	        int num = 2;

	        while (count < n) {
	            if (isPrime(num)) {
	                sum += num * num;
	                count++;
	            }
	            num++;
	        }

	        System.out.println("Sum of squares of the first " + n + " prime numbers: " + sum);
	    }
	    public static boolean isPrime(int num) {
	        if (num <= 1) {
	            return false;
	        }
	        for (int i = 2; i <= Math.sqrt(num); i++) {
	            if (num % i == 0) {
	                return false;
	            }
	        }
	        return true;
	    }


	
	
//	public static void main(String args[]) {
//		List<Integer> list = new ArrayList<>();
//		int i;
//		int count=0;
//		for(int j=1;j<=10; j++) {
//		for(i=1; i<=j; i++) {
//		if(j%i==0) {
//			count++ ;
//		}
//		}
//		if(count==2)
//			list.add(null);
//		}
//		System.out.println(list);
//		}
	
	}

