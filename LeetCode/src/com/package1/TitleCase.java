package com.package1;

public class TitleCase {

	    public static void main(String[] args) {
	        String input = "HELLO WORLD";
	        
	        String titleCaseString = convertToTitleCase(input);
	        System.out.println(titleCaseString);
	    }

	    public static String convertToTitleCase(String input) {
	        if (input == null || input.isEmpty()) {
	            return input;
	        }

	        StringBuilder titleCase = new StringBuilder(input.length());
	        boolean nextTitleCase = true;

	        for (char c : input.toCharArray()) {
	            if (Character.isSpaceChar(c)) {
	                nextTitleCase = true;
	            } else if (nextTitleCase) {
	                c = Character.toTitleCase(c);
	                nextTitleCase = false;
	            } else {
	                c = Character.toLowerCase(c);
	            }
	            titleCase.append(c);
	        }

	        return titleCase.toString();
	    }
	}


