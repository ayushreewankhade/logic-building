package threading;

import java.util.ArrayList;
import java.util.List;

public class FindElement {

	    public static void main(String[] args) {
	        List<Integer> numbers = new ArrayList<>();
	        for (int i = 1; i <= 100; i++) {
	            numbers.add(i);
	        }
            int target =45;
	        int midpoint = numbers.size() / 2;
	        List<Integer> firstHalf = numbers.subList(0, midpoint);
	        List<Integer> secondHalf = numbers.subList(midpoint, numbers.size());

	        // Create two threads and pass the sublists for processing
	        Thread thread1 = new Thread(new ListProcessor(firstHalf, target));
	        Thread thread2 = new Thread(new ListProcessor(secondHalf,target));

	        thread1.start();
	        thread2.start();

	        try {
	            thread1.join();
	            thread2.join();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }

//	        System.out.println("Processing of both sublists is complete.");
	    }

	    static class ListProcessor implements Runnable {
	        private final List<Integer> list;
            private final int target;
            
	        public ListProcessor(List<Integer> list,int target) {
	            this.list = list;
	            this.target = target;
	        }

	        @Override
	        public void run() {
	            for (Integer num : list) {
	            	if(num==target) {
	            		System.out.println(num);
	            	}
	                // Process each element in the list here
//	                System.out.println("Processing: " + num + " in Thread: " + Thread.currentThread().getName());
	            }
	        }
	    }
	}


