package threading;

	public class SynchronizedCounter implements Runnable {
	    private int count = 0;

	    public void run() {
	        for (int i = 0; i < 5; i++) {
	            increment();
	            try {
	                Thread.sleep(3000); // Sleep for 1 minute (60,000 milliseconds)
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }// Call the synchronized method to update the counter safely
//	          System.out.println(getCount()); 
	        }
	    }

	    // Synchronized method to increment the counter safely
	    public synchronized void increment() {
	        count++;
	    }

	    // Synchronized method to get the current value of the counter
	    public synchronized int getCount() {
	        return count;
	    }

	    public static void main(String[] args) {
	        SynchronizedCounter counter = new SynchronizedCounter();

	        // Creating and starting multiple threads
	        Thread thread1 = new Thread(counter);
	        Thread thread2 = new Thread(counter);

	        thread1.start();
	        try {
	            thread1.join();
//	            thread2.join();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	        int finalCount1 = counter.getCount();
	        System.out.println("count after 1st thread execution: " + finalCount1);
	        thread2.start();
	        try {
//	            thread1.join();
	            thread2.join();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }

	        // Get the final value of the counter
	        int finalCount = counter.getCount();
	        System.out.println("Final count: " + finalCount);
	    }
	}


